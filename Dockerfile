FROM php:7.4-apache

WORKDIR /var/www/html

RUN  apt-get update && apt-get -y install gnupg2 gnupg gnupg1

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - \
  && apt-get update && apt-get -y install git zip unzip nodejs yarn

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

RUN pecl install xdebug \
 && docker-php-ext-enable xdebug \
 && apt-get install -y libpq-dev \
 && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
 && docker-php-ext-install -j$(nproc) mysqli pgsql pdo pdo_mysql pdo_pgsql

COPY docker/apache.conf /etc/apache2/sites-enabled/apache.conf
RUN a2enmod headers && rm -rf /var/lib/apt/lists/*
