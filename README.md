## SETUP

Clone the repo locally:

```sh
$ git clone git@bitbucket.org:loru88/skytechtest.git
$ cd skytechtest
```

You can use Docker for bootstrapping a quick environment and setup the project

```sh
$ docker-compose up -d
```

Install the dependencies and ingest fake data

```sh
$ docker-compose exec app bash
$ composer install
$ bin/console doctrine:database:create
$ bin/console doctrine:migrations:migration
$ bin/console test:data-ingestion
```

## USAGE

Request metrics for CPU load between 06/02/2020 01:15:00 and 06/02/2020 01:18:00

```
http://localhost:8000/metrics?event-name=cpu-load&end=2020-02-06T01:18:00&start=2020-02-06T01:15:00
```

Request metrics for concurrency from 06/02/2020 01:15:00 until current time

```
http://localhost:8000/metrics?event-name=concurrency&start=2020-02-06T01:15:00
```

Request metrics for concurrency from the beginning up to 06/02/2020 01:15:00

```
http://localhost:8000/metrics?event-name=concurrency&end=2020-02-06T01:15:00
```

Simple front end in React

```sh
$ cd frontend
$ yarn install
$ yarn start
```

## THOUGHTS

For sake of simplicity and quicker setup, the .env file is committed with the credentials used in Docker.
A global exception listener would be nicer to handle error messages to show.
I used TimescaleDB instead of MySQL because is better for time series and the syntax is the same as Postgres.
InfluxDB would be another choice or also NoSQL databases.

This is not a production ready application, for example CORS is disabled for the sake of the test and the backend and
frontend are not built for production. Same for the Docker image which is not optimized for production.
Also the backend URL is hardcoded, but I would solve this by serving the UI under the same domain of the backend,
achievable in many way one of them by using a reverse proxy/load balancer in front of both frontend and backend. 

Every time you down the container data is lost, hence you need to ingest it again.
There is not time picker selector to change range, hence you need to ingest data again.

Test would be useful in backend to ensure the out coming JSON is always as expected and in the frontend would be helpful
to test if UI components are visible and respond nicely to any issue.

I would use Typescript for a bigger project because of it's type safety and modern features.
