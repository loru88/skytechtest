<?php

namespace App\Controller;

use App\Entity\DataPoint;
use App\Metrics\MetricsRepository;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MetricsController extends AbstractController
{
    /**
     * @Route("/metrics", name="metrics")
     * @param Request $request
     * @param MetricsRepository $metrics_repository
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index(
        Request $request,
        MetricsRepository $metrics_repository
    ) {

        $params = $request->query->all();

        return $this->json($metrics_repository->findBy(
            $params
        ));
    }
}
