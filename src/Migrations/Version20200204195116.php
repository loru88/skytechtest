<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200204195116 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;');
        $this->addSql( 'CREATE TABLE data_point (time TIMESTAMPTZ NOT NULL, event TEXT NOT NULL, value DOUBLE PRECISION NOT NULL)');
        $this->addSql('SELECT create_hypertable(\'data_point\', \'time\');');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE data_point');
    }
}
