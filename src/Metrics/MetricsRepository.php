<?php

namespace App\Metrics;

use App\Entity\DataPoint;
use DateInterval;
use DateTimeImmutable;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\ParameterType;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MetricsRepository
{
    private Connection $connection;
    private OptionsResolver $optionsResolver;

    public function __construct(Connection $connection)
    {
        $this->connection      = $connection;
        $this->optionsResolver = new OptionsResolver();
        $this->configureOptions();
    }

    public function findBy(array $params)
    {

        $options = $this->optionsResolver->resolve($params);

        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();

//        $qb->select('event, to_char(time::DATE, \'dd/mm/yyyy\'), value')
        $qb->select('event, time, value')
           ->from('data_point')
           ->orderBy('time', 'DESC');

        $qb->where('event = ?')
           ->setParameter(0, $options['event-name'], ParameterType::STRING);

        $qb->andWhere('time > ?')
           ->setParameter(1, $options['start'], ParameterType::STRING);


        $qb->andWhere('time < ?')
           ->setParameter(2, $options['end'], ParameterType::STRING);


        return $qb->execute()->fetchAll();
    }

    private function configureOptions()
    {
        $this->optionsResolver->setDefined([
            'event-name',
            'start',
            'end'
        ]);

        $this->optionsResolver->setDefaults([
            'end' => (new DateTimeImmutable())->format('c')
        ]);

        $this->optionsResolver->setRequired('event-name');
        $this->optionsResolver->setAllowedTypes('event-name', 'string');
        $this->optionsResolver->setAllowedValues('event-name', ['cpu-load', 'concurrency']);

        $dateNormalizer = function (Options $options, $value) {
            if (is_string($value) && ! empty($value)) {
                return (new DateTimeImmutable($value))->format('c');
            }

            return (new DateTimeImmutable())->format('c');
        };

        $this->optionsResolver->setNormalizer('end', $dateNormalizer);
        $this->optionsResolver->setNormalizer('start', $dateNormalizer);
        $this->optionsResolver->setDefault('start', function (Options $options) {
            return (new DateTimeImmutable($options['end']))->sub(new DateInterval('PT5M'))->format('c');
        });

    }

}
