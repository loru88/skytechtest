<?php

namespace App\Command;

use App\Entity\DataPoint;
use DateTimeImmutable;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Statement;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DataIngestionCommand extends Command
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(
        Connection $connection
    ) {
        parent::__construct("test:data-ingestion");
        $this->connection = $connection;
    }

    public function configure()
    {
        $this->setDescription("Ingest fake metrics data for the sake of the tech test");
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $stmt     = $this->connection->prepare('INSERT INTO data_point VALUES (?,?,?);');
        $now      = (new DateTimeImmutable());
        $start    = $now->getTimestamp();
        $interval = 300;

        for ($i = $start - $interval; $i <= $start; $i++) {

            $timestamp = $this->randomTimestamp($i);
            $dataPoint = $this->createDataPoint($timestamp);
            $this->insert($dataPoint, $stmt);


            $dataPoint->setTime($timestamp);
            $this->insert($dataPoint, $stmt);


            $dataPoint->setTime($timestamp);
            $dataPoint->setEvent('concurrency');
            $dataPoint->setValue(mt_rand(0, 500000));

            $stmt->execute(
                [
                    $dataPoint->getTime()->format('c'),
                    $dataPoint->getEvent(),
                    $dataPoint->getValue()
                ]
            );
        }

        $output->writeln('Random fake data ingested for the past 5 minutes');

        return 0;
    }

    private function randomTimestamp($max)
    {
        $randomTimestamp = $max - mt_rand(0, 5);
        return (new DateTimeImmutable())->setTimestamp($randomTimestamp);
    }

    private function createDataPoint(DateTimeImmutable $timestamp)
    {
        $dataPoint = new DataPoint();
        $dataPoint->setTime($timestamp);
        $dataPoint->setEvent('cpu-load');
        $dataPoint->setValue(mt_rand(0, 100));
        return $dataPoint;
    }

    private function insert(DataPoint $dataPoint, \Doctrine\DBAL\Driver\Statement $stmt)
    {
        $stmt->execute(
            [
                $dataPoint->getTime()->format('c'),
                $dataPoint->getEvent(),
                $dataPoint->getValue()
            ]
        );
    }
}
