import React from 'react';
import './App.css';
import {Chart} from './Chart';

function App() {
  return (
    <div className="App">
        <Chart event={"cpu-load"} title={"CPU load"} max_y={100} />
        <Chart event={"concurrency"} title={"Concurrency"} max_y={500000} />
    </div>
  );
}

export default App;
