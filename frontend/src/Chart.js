import React from 'react';
import MetricsGraphics from 'react-metrics-graphics';
import 'metrics-graphics/dist/metricsgraphics.css';

export class Chart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        const start = new Date();
        start.setTime(start.getTime() - 60*60*1000); // 1 hour ago

        fetch('http://localhost:8000/metrics?event-name=' + this.props.event + '&start=' + start.toISOString())
            .then(res => res.json())
            .then(result => result.map((x) => {
                x.time = new Date(x.time); // transform into a Date object
                return x;
            }))
            .then(
                (result) => {

                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else if (items.length === 0) {
            return <div>No data to show</div>;
        } else {
            return (
                <MetricsGraphics
                    title={this.props.title}
                    data={items}
                    width={800}
                    height={400}
                    x_sort={true}
                    x_accessor="time"
                    y_accessor="value"
                    max_y={this.props.max_y}
                />
            );
        }
    }
}
